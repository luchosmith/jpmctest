# JPMC Code Test

* define an API schema
	* see schema.json file 	
* write OO code using TDD
	* run npm install
	* run jasmine
* spec out high level components
	* see product-list-components.txt file
