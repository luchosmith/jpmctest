const Calculator = require('../src/calculator')
const delay = require('../src/delay')

describe( "delay", () => {
    var calculator = new Calculator();

    it( "returns a promise", function(){
      var willAdd = delay( 100, calculator, 'add', [ 1, 1 ] );
        expect(willAdd).toEqual(jasmine.any(Promise));
        expect( willAdd ).toBe.fulfilled;
    });

    it( "delays execution", (done) => {
        delay( 1000, calculator, 'add', [ 10, 5 ] ).then ((result) => {
            expect(result).toEqual(15)
            done()
        })
        delay( 500, calculator, 'subtract', [ 9, 5 ] ).then ((result => {
            expect(result).toEqual(4)
            done()
        }))
    });

    it( "cannot execute functions that do not exist", () => {
      expect( delay( 1000, calculator, 'sqrt', [ 2, 2 ] ) ).toBe.rejected;
    });
});