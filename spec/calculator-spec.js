const Calculator = require('../src/calculator')
const ScientificCalculator = require('../src/scientific-calculator')

describe( "Calculator", () => {
    var calculator;
    beforeEach( () => {
      calculator = new Calculator();
    } );
    
    it( "adds 1 and 2", () => {
      expect( calculator.add(1,2) ).toEqual( 3 );
    } );

    it( "subtracts 2 from 9", () => {
      expect( calculator.subtract( 9, 2 ) ).toEqual( 7 );
    } );

    it( "multiplies 4 and 3", () => {
      expect( calculator.multiply( 4, 3 ) ).toEqual( 12 );
    } );

    it( "divides 10 by 2", () => {
      expect( calculator.divide( 10, 2 ) ).toEqual( 5 );
    } );

    it( "does not divide by 0", () => {
      expect( calculator.divide( 5, 0 ) ).toBeNaN();
    } );

    it( "throws when an invalid number is passed in", () => {
        expect(function() { calculator.add("a","b") }).toThrow();
    });
   } );
   