const Calculator = require('../src/calculator')
const mixin = require('../src/with-exponents')


describe( "withExponents", () => {
    var calculator;
    beforeEach( () => {
      calculator = new Calculator();
      mixin.withExponents.call( calculator );
    } );

    it( "returns 2^3", () => {
    expect( calculator.pow( 2, 3 ) ).toEqual( 8 );
    } );

    it( "multiplies 2^3 and 2^4", () => {
    expect( calculator.multiplyExp( [ 2, 3 ], [ 2, 4 ] ) ).toEqual( 128 );
    } );

    it( "divides 2^3 by 2^5", () => {
    expect( calculator.divideExp( [ 2, 3 ], [ 2, 5 ] ) ).toEqual( 0.25 );
    } );
   } );