const Calculator = require('../src/calculator')
const ScientificCalculator = require('../src/scientific-calculator')

describe( "Scientific Calculator", () => {
    var calculator;
    beforeEach( () => {
        calculator = new ScientificCalculator();
    } );

    it( "extends Calculator", () => {
        expect(calculator).toEqual(jasmine.any(Calculator));
        expect(calculator).toEqual(jasmine.any(ScientificCalculator));
    } );

    it( "returns the sine of PI / 2", () => {
        expect( calculator.sin( Math.PI / 2 ) ).toEqual( 1 );
    } );

    it( "returns the cosine of PI", () => {
        expect( calculator.cos( Math.PI ) ).toEqual( -1 );
    } );

    it( "returns the tangent of 0", () => {
        expect( calculator.tan( 0 ) ).toEqual( 0 );
    } );

    it( "returns the logarithm of 1", () => {
        expect( calculator.log( 1 ) ).toEqual( 0 );
    } );
 
    it( "returns NaN for the logarithm of -1", () => {
        expect( calculator.log( -1 ) ).toBeNaN();
    } );
} );
