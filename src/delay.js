module.exports = function(duration, obj, fn, args) {
    return new Promise(function(resolve, reject){
        setTimeout(function(){
            if(obj[fn] && typeof obj[fn] === 'function') {
                resolve(obj[fn](...args));
            }else {
                reject()
            }
        }, duration)
    });
};
