module.exports = class Calculator {

    add(a,b) {
        this._validate(a,b)
        return Number(a) + Number(b)
    }
    subtract(a,b) {
        this._validate(a,b)
        return Number(a) - Number(b)
    }
    multiply(a,b) {
        this._validate(a,b)
        return a*b
    }
    divide(a,b) {
        this._validate(a,b)
        if (Number(b) === 0) {
            return NaN
        }
        return a/b
    }

    _validate(...nums) {
        var valid = nums.every( (n) => {
            return !isNaN(parseFloat(n))
        })
        if (!valid) {
            throw Error('invalid number')
        }
    }
}

