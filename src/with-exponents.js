const Calculator = require('./calculator')

module.exports = {

    withExponents: function () {

        if (!(this instanceof Calculator)) {
            throw Error('This function requires Calculator functionality')
        }

        this.pow = function(a,b) {
            this._validate(a,b)
            return Math.pow(a,b)
        }
    
        this.multiplyExp = function(a,b) {
            if (Array.isArray(a) && Array.isArray(b)) {
                this._validate(...a,...b)
                return this.multiply(this.pow(a[0],a[1]), this.pow(b[0],b[1]))
            } else {
                throw Error('invalid arguments')
            }
    
        }
    
        this.divideExp = function(a,b) {
            if (Array.isArray(a) && Array.isArray(b)) {
                this._validate(...a,...b)
                return this.divide(this.pow(a[0],a[1]), this.pow(b[0],b[1]))
            } else {
                throw Error('invalid arguments')
            }
        }
    }
  };
