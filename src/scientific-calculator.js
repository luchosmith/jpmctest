const Calculator = require('./calculator')

module.exports = class ScientificCalculator extends Calculator {

    sin(radians) {
        this._validate(radians)
        return Math.sin(radians)
    }

    cos(radians) {
        this._validate(radians)
        return Math.cos(radians)
    }

    tan(radians) {
        this._validate(radians)
        return Math.tan(radians)
    }

    log(x) {
        this._validate(x)
        return Math.log(x)
    }

}
